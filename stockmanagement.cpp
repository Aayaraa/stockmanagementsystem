#include <iostream>
#include <fstream>
#include <conio.h>
#include <bits/stdc++.h>
using namespace std;

class Item
{
    int itemId, quantity;
    char title[25], type[10];
    float price;

  public:
    Item()
    {
        this->itemId = 0;
        this->quantity = 0;
        strcpy(title, "title");
        strcpy(type, "type");
        this->price = 0;
    }

    void getItemData()
    {
        cout << "Enter Item ID\t";
        cin >> itemId;
        cin.ignore();
        cout << "Enter Item Title:\t";
        cin.getline(title, 25);
        cout << "Enter Item Type(eg:CD, DVD, Magazine):\t";
        cin.getline(type, 10);
        cout << "Enter Price:\t";
        cin >> price;
        cout << "Enter quantity:\t";
        cin >> quantity;

        //			cout<<itemId<<endl<<title<<endl<<type<<endl<<price<<endl;
    }

    void storeItem()
    {
        if (itemId > 0 && price > 0)
        {
            //create an object of class ofstream to write to file
            ofstream file;
            //create a file if it doesn't exist and open it in append mode
            file.open("music_shop_stock_details.txt", ios::app);
            //write to file
            file.write((char *)this, sizeof(*this));
            //close the file
            file.close();
            cout << "Data inserted successfully\n";
        }
        else
            cout << "Invalid Input\nData NOT INITIALIZED\n";
    }

    void displayItemData()
    {
        cout << itemId << "\t"
             << title << "\t\t"
             << type << "\t\t"
             << price << "\t\t"
             << quantity << endl;
    }

    void viewAllItems()
    {
        //create an object of class ifstream to read from file
        ifstream file;
        //open the file
        file.open("music_shop_stock_details.txt");

        //make sure the file exists and is open
        if (!file)
            cout << "File not found\n";
        else
        {
            file.read((char *)this, sizeof(*this));
            cout << "ID\tTITLE\t\tTYPE\t\tPRICE\t\tIN STOCK\n";
            //loop through the contents of file until the end is reached
            while (!file.eof())
            {
                displayItemData();
                file.read((char *)this, sizeof(*this));
            }
            file.close();
        }
    }

    void searchItem(char *name)
    {
        bool found = false;
        //create an object of class ifstream to read from file for performing search operation
        ifstream file;
        //open file
        file.open("music_shop_stock_details.txt");

        //make sure the file exists and is open
        if (!file)
            cout << "File not found\n";
        else
        {
            file.read((char *)this, sizeof(*this));

            //loop through the contents of file until the end is reached
            while (!file.eof())
            {
                //if the user input title is found
                if (!strcmp(name, title))
                {
                    displayItemData();
                    found = true;
                }
                file.read((char *)this, sizeof(*this));
            }
            if (!found)
                cout << "Item not found\n";
            file.close();
        }
    }

    void removeItem(char *name)
    {
        //defining a boolean variable to track whether a match is found or not and initializing it with value false
        bool found = false;
        int sure_to_proceed;
        //Ask the user for confirmation
        cout << "Are you sure you want to delete the item \""
             << name
             << "\" permanently?"
             << "\nThis process is IRREVERSIBLE\n"
             << "Enter 1 to proceed, 0 to cancel: \t";
        cin >> sure_to_proceed;
        if (sure_to_proceed)
        {
            //create an object of class ifstream to read from file
            ifstream file_in;
            //create an object of class ofstream to write changes to file
            ofstream file_out;
            file_in.open("music_shop_stock_details.txt");
            //make sure the file to read from exists and is open
            if (!file_in)
                cout << "File not found";
            else
            {
                //create a temporary file in append mode
                file_out.open("temp_file.txt", ios::app);
                file_in.read((char *)this, sizeof(*this));
                //loop through contents of the file to read from until the end is reached
                while (!file_in.eof())
                {
                    //note: strcmp returns 0 when match is found
                    //write content to a temporary file except for the match
                    if (strcmp(name, title))
                        file_out.write((char *)this, sizeof(*this));
                    else //match found
                        found = true;
                    file_in.read((char *)this, sizeof(*this));
                }
                //close both files
                file_in.close();
                file_out.close();
                //remove the file from which the data was read
                remove("music_shop_stock_details.txt");
                //rename the temporary file so it is the main file to keep the data in later executions of the program
                rename("temp_file.txt", "music_shop_stock_details.txt");
                //inform user the result of the operation
                if (!found)
                    cout << "Item not found\nOperation UNSUCCESSFUL\n";
                else
                    cout << "Item deleted Successfully\n";
            }
        }
        else
            cout << "Operation Cancelled by user\n";
    }

    void updateItem(char *name)
    {
        bool updated = false;
        //create an object of class fstream to perform both read and write operations
        fstream file;
        file.open("music_shop_stock_details.txt", ios::in | ios::out | ios::ate);
        //make sure the fie exists and is open
        if (!file)
            cout << "File not found\n";

        else
        {
            //move the pointer to the beginning of the file before starting the read
            file.seekg(0);
            file.read((char *)this, sizeof(*this));
            //loop through contents of the file to read from until the end is reached
            while (!file.eof())
            {
                if (!strcmp(name, title))
                { //match found
                    //ask user for the updated values
                    getItemData();
                    //get the cursor's position in the file, and move it towards left so as to write the updated values
                    file.seekp(file.tellp() - sizeof(*this));
                    //write the changes
                    file.write((char *)this, sizeof(*this));
                    updated = true;
                }
                file.read((char *)this, sizeof(*this));
            }
            file.close();
        }
        if (updated)
            cout << "Operation Successful\n";
    }

    void changeQuantity(char *name, int quan)
    {
        //create an object of class fstream to perform read and write operations
        fstream file;
        file.open("music_shop_stock_details.txt");
        //make sure file exists and is open
        if (!file)
            cout << "File not found\n";
        else
        {
            file.read((char *)this, sizeof(*this));
            //loop through contents of the file to read from until the end is reached
            while (!file.eof())
            {
                if (!strcmp(name, title))
                { //match found
                    //change the quantity of the item
                    this->quantity += quan;
                    //get the cursor at the desired position before writing changes
                    file.seekp(file.tellp() - sizeof(*this));
                    //write changes to the file
                    file.write((char *)this, sizeof(*this));
                }
                file.read((char *)this, sizeof(*this));
            }
            file.close();
            cout << "Operation Successful\n";
        }
    }

    void maintainSalesReport(char *name, int q)
    {
        //create an object of class oftream to write to file
        ofstream file;
        //create a file if it doesn't exist and open it in append mode
        file.open("music_shop_sales_report.txt", ios::app);
        //make sure the file exists and is open
        if (!file)
            cout << "File not found\n";
        //write to file
        file << name << "\t" << q << endl;
        //close the file
        file.close();
    }

    void displaySalesReport()
    {
        //create an object of class iftream to perform read operations
        ifstream file;
        //create a string to read from file and keep
        string report;
        //open the file
        file.open("music_shop_sales_report.txt");
        //make sure the file exists and is open
        if (!file)
            cout << "File not found\n";
        //loop through the contents of the file until the end is reached
        while (!file.eof())
        {
            //get one whole line from file and keep the data in the previously declared string
            getline(file, report);
            //print that line to terminal
            cout << report << endl;
        }
        //close the file
        file.close();
    }

    void sellItem(char *n, int q)
    {
        //create object of class ifstream to read from files and open the file
        ifstream file;
        file.open("music_shop_stock_details.txt");
        //make sure the file exists and is open
        if (!file)
            cout << "File not found\n";
        else
        {
            file.read((char *)this, sizeof(*this));
            //loop through the contents of the file until the end is reached
            while (!file.eof())
            {
                if (!strcmp(n, title))
                { //match found
                    if (this->quantity < q)
                        cout << "NOT ENOUGH items in stock\n";
                    else
                    {
                        maintainSalesReport(n, q);
                        changeQuantity(n, -q); //call another function to actually write changes to the file
                    }
                    break;
                }
                file.read((char *)this, sizeof(*this));
            }
            file.close();
        }
    }
    void restockItem(char* n, int q){
        changeQuantity(n,q);
    }
};

int showMenu()
{
    int choice;
    cout << "CD/DVD/Magazine STOCK MANAGEMENT\n"
         << "What would you like to do?\n"
         << "1.Add an item\n"
         << "2.View all items in stock\n"
         << "3.Search an item\n"
         << "4.Sell an item\n"
         << "5.Re-stock an item\n"
         << "6.Remove an item\n"
         << "7.Update an existing item details\n"
         << "8.View sales report\n"
         << "9.Exit program\n";
    cin >> choice;
    return choice;
}

int main()
{
    Item item;
    char title[25];
    int quantity;

    while (true)
    {
        system("cls");
        switch (showMenu())
        {
        case 1:
            item.getItemData();
            item.storeItem();
            break;

        case 2:
            item.viewAllItems();
            break;

        case 3:
            cout << "Enter title of the item to search:\t";
            cin.ignore();
            cin.getline(title, 25);
            item.searchItem(title);
            break;

        case 4:
            cout << "Enter title of item to sell:\t";
            cin.ignore();
            cin.getline(title, 25);
            cout << "Enter the number of \"" << title << "\" to sell:\t";
            cin >> quantity;
            item.sellItem(title, quantity);
            break;

        case 5:
            cout << "Enter title of item to re-stock:\t";
            cin.ignore();
            cin.getline(title, 25);
            cout << "Enter the number of \"" << title << "\" to re-stock:\t";
            cin >> quantity;
            item.restockItem(title, quantity);
            break;

        case 6:
            cout << "Enter the title of the item to remove:\t";
            cin.ignore();
            cin.getline(title, 25);
            item.removeItem(title);
            break;

        case 7:
            cout << "Enter the title of the item to update:\t";
            cin.ignore();
            cin.getline(title, 25);
            item.updateItem(title);
            break;

        case 8:
            item.displaySalesReport();
            break;

        case 9:
            int sure;
            cout << "Are you sure you want to exit?\n"
                 << "Enter 1 to proceed, 0 to cancel:\t";
            cin >> sure;
            if (sure)
                exit(0);
            break;

        default:
            cout << "INVALID INPUT" << endl;
        }
        getch();
    }
}